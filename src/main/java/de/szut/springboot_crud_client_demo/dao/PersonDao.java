package de.szut.springboot_crud_client_demo.dao;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import de.szut.springboot_crud_client_demo.model.Person;


public class PersonDao {
	 
    private final String URL_PERSONS = "http://localhost:8080/SpringBootCrudService/allpersons";
    private final String URL_PERSON = "http://localhost:8080/SpringBootCrudService/person";
    private final String URL_PERSON_URL = URL_PERSON + "/";
    private final String URL_PERSON_PARAM = URL_PERSON + "?id=";

    private final MediaType MEDIA_TYPE = MediaType.APPLICATION_JSON;

    /**
     * Legt einen neuen Datensatz für eine Person an.
     * @param person Die zu persistierende Person.
     */
    public void create(Person person) {
        // HTTP-Request erstellen.
    	HttpHeaders headers = createHttpHeaders();
        HttpEntity<Person> requestEntity = new HttpEntity<Person>(person, headers);
        // Objekt für synchrone REST-Anfrage erstellen.
        RestTemplate restTemplate = new RestTemplate();
        // Anfrage senden mit POST-Methode.
        ResponseEntity<Person> responseEntity = restTemplate.postForEntity(URL_PERSON, requestEntity, Person.class);
        // HTTP-Status auslesen und ausgeben.
        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode == HttpStatus.OK) {
            // Daten aus dem Body der Antwort auslesen und ausgeben.
        	person = responseEntity.getBody();
        }
        else {
        	System.out.println("Fehler: " + statusCode);
        	person = null;
        }
    }

    /**
     * Liest alle Datensätze.
     * @return Alle persistierten Personen.
     */
    public List<Person> readAll() {
        // HTTP-Request erstellen.
    	HttpHeaders headers = createHttpHeaders();
        HttpEntity<Person[]> requestEntity = new HttpEntity<Person[]>(headers);
        // Objekt für synchrone REST-Aufrufe erstellen.
        RestTemplate restTemplate = new RestTemplate();
        // Anfrage senden mit GET-Methode.
        ResponseEntity<Person[]> responseEntity = restTemplate.exchange(URL_PERSONS, HttpMethod.GET, requestEntity, Person[].class);
        // HTTP-Status auslesen und ausgeben.
        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode == HttpStatus.OK) {
            // Daten aus dem Body der Antwort auslesen und ausgeben.
            Person[] personen = responseEntity.getBody();
            List<Person> list = Arrays.asList(personen);
            return list;
        }
        else {
        	System.out.println("Fehler: " + statusCode);
        	return null;
        }
    }

    /**
     * Liest eine Person.
     * @param id    Die ID der gesuchten Person.
     * @return  Die gesuchte Person oder NULL.
     */
    public Person readByUrl(int id) {
    	return read(id, URL_PERSON_URL);
    }

    /**
     * Liest einePerson.
     * @param id    Die ID der gesuchten Person.
     * @return  Die gesuchte Person oder NULL.
     */
    public Person readByParam(int id) {
    	return read(id, URL_PERSON_PARAM);
    }

    /**
     * Liest eine Person, spezifiziert durch ihre ID, die als Teil der URL mitgegeben wird.
     * @param id    Die ID der gesuchten Person.
     * @param url   Die zu verwendende URL.
     * @return Die gesuchte Person.
     */
    private Person read(int id, String url) {
        // HTTP-Request erstellen.
    	HttpHeaders headers = createHttpHeaders();
        HttpEntity<Person> requestEntity = new HttpEntity<Person>(headers);
        // Objekt für synchrone REST-Aufrufe erstellen.
        RestTemplate restTemplate = new RestTemplate();
        // Anfrage senden mit GET-Methode.
        url = url + id;
        ResponseEntity<Person> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Person.class);
        // HTTP-Status auslesen und auswerten.
        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode == HttpStatus.OK) {
            // Daten aus dem Body der Antwort auslesen und ausgeben.
            Person person = responseEntity.getBody();
            return person;
        }
        else {
        	System.out.println("Fehler: " + statusCode);
        	return null;
        }
    }

    /**
     * Aktualisiert einen bereits bestehende Person.
     * @param person    Die zu aktualisierende Person.
     */
    public void update(Person person) {
        // HTTP-Request erstellen.
        HttpHeaders headers = createHttpHeaders();
        HttpEntity<Person> requestEntity = new HttpEntity<Person>(person, headers);
        // Objekt für synchrone REST-Aufrufe erstellen.
        RestTemplate restTemplate = new RestTemplate();
        // Anfrage senden mit PUT-Methode.
        ResponseEntity<Void> responseEntity = restTemplate.exchange(URL_PERSON, HttpMethod.PUT, requestEntity, Void.class);
        // HTTP-Status auslesen und auswerten.
        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode != HttpStatus.OK) {
        	System.out.println("Fehler: " + statusCode);
        }
    }

    /**
     * Löscht eine Person, spezifiziert durch ihre ID, die als Teil der URL mitgegeben wird.
     * @param id    Die ID der zu löschenden Person.
     */
    public void deleteByUrl(int id) {
        delete(id, URL_PERSON_URL);
    }

    /**
     * Löscht eine Person, spezifiziert durch ihre ID, die als Parameter mitgegeben wird.
     * @param id    Die ID der zu löschenden Person.
     */
    public void deleteByParam(int id) {
        delete(id, URL_PERSON_PARAM);
    }

    /**
     * Löscht eine Person.
     * @param id    Die ID der zu löschenden Person.
     * @param url   Die zu verwendende URL.
     */
    private void delete(int id, String url) {
        // Objekt für synchrone REST-Aufrufe erstellen.
        RestTemplate restTemplate = new RestTemplate();
        // URL aufbauen.
        url = url + id;
        // DELETE-Anfrage senden.
        restTemplate.delete(url);
    }

    /**
     * Erstellt einen Http-Header.
     * @return Der erstellte Http-Header
     */
    private HttpHeaders createHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(new MediaType[] { MEDIA_TYPE }));
        headers.setContentType(MEDIA_TYPE);
        return headers;
    }
 
}
